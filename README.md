# This is module which calculate hash sum target directory
[![Build Status](https://gitlab.com/7bars/hasher/badges/master/build.svg)](https://gitlab.com/7bars/hasher/commits/master) [![Coverage Report](https://gitlab.com/7bars/hasher/badges/master/coverage.svg)](https://gitlab.com/7bars/hasher/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/7bars/hasher)](https://goreportcard.com/report/gitlab.com/7bars/hasher) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

## Work only 1 thread