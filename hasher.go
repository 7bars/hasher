package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

type hashSumFiles struct {
	Table []hashSumFile `json:"table"`
}

type hashSumFile struct {
	Path string `json:"path"`
	Sum  string `json:"sum"`
}

//hashSumRecursive accept directory
//return struct hashSumFiles and error
//Calculating hash sum recursive only 1 thread
func hashSumRecursive(dir string) (hashSumFiles, error) {
	var table hashSumFiles
	err := filepath.Walk(dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.Mode().IsRegular() {
				b, err := hashSum(path)
				if err != nil {
					fmt.Println(errors.Cause(err))
				} else {
					table.Table = append(table.Table, hashSumFile{path, fmt.Sprintf("%x", b)})
				}
			}
			return nil
		})

	if err != nil {
		log.Println(err)
	}
	return table, nil
}

//hashSum input path to file, calculates hash sum via md5
//and return []byte and error
func hashSum(path string) ([]byte, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return nil, err
	}

	return hash.Sum(nil), nil
}
