package main

import (
	"fmt"
	"testing"
)

//TestHashSum check hash sum file "test.file"
func TestHashSum(t *testing.T) {
	testHash := "bee49345af0f52e5c0bb717432593545"
	b, err := hashSum("test.file")
	if err != nil {
		t.Errorf("Expected 'nil', but got '%s'", err)
	}
	hashStr := fmt.Sprintf("%x", b)
	if hashStr != testHash {
		t.Errorf("Expected '"+testHash+"', but got '%s'", hashStr)
	}
}

//TestHashSumRecursive check only error
func TestHashSumRecursive(t *testing.T) {
	_, err := hashSumRecursive(".")
	if err != nil {
		t.Errorf("Expected 'nil', but got '%s'", err)
	}
}
